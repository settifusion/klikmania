#SingleInstance, Force
SetWorkingDir, %A_ScriptDir%
SendMode Input
param = %1%
; if (param <> "passed")
; {
; MsgBox,64,, "Opis znaków i ich wprowadzania`ndostępny jest po naciśnięciu klawiszy Win+F1", 2
; }

if ( ! A_IsCompiled) ;; jeśli nie jest skompilowany podepnij ikonę. Do kompilacji trzeba ją podpiąć ręcznie. 
{
Menu, Tray, Icon,,,1
Menu, Tray, Icon, %A_ScriptDir%\ico\klikmania.ico,1,1
}

SetKeyDelay, -1, -1

Menu, Tray, Add  ; Creates a separator line.
Menu, Tray, Add, IconHelp, tuHelp  ; Creates a new menu item.
Menu, Tray, Default , IconHelp
Menu, Tray, Click, 1

IfNotExist, %USERPROFILE%\pdftotext
   FileCreateDir, %USERPROFILE%\pdftotext

FileInstall, D:\pparaf\Projekty\klikmania\pdftotext\pdftotext.exe, %USERPROFILE%\pdftotext\pdftotext.exe

^+b:: ;; bibliografia - wyciągnięcie tekstu
ShowGui:
Gui, PDF:Font, s10, Verdana  ; Set 10-point Verdana
Gui, PDF:Add, Edit,   x10 y10 w450  h20 vName Center
Gui, PDF:Add, Button, x465 y10 w40 h20 vPlik gPlik, Plik
Gui, PDF:Add, Edit,   x80 y45 w80  h20 vFrom Center
Gui, PDF:Add, Edit,   x270 y45 w80  h20 vTo Center
Gui, PDF:Add, Text,   x10 y48 w65 h20 , Od strony
Gui, PDF:Add, Text,   x200 y48 w65 h20 , Do strony
Gui, PDF:Add, Button, x132 y80 w60  h25 gAnuluj, Anuluj
Gui, PDF:Add, Button, x262 y80 w80  h25 gZapisz, Konwertuj


Gui, PDF:Show, x550 y400 AutoSize, Konwersja PDF na tekst
Return

GuiPDFClose:
Gui, PDF:Destroy
return

Plik:
FileSelectFile, NazwaPliku,3,, Wybierz plik PDF, *.pdf
GuiControl,, Name, %NazwaPliku%
return

Zapisz:
GuiControlGet, Name, PDF:
GuiControlGet, From, PDF:
GuiControlGet, To, PDF:
If (From = "")
{
	msgbox Nie podano początku zakresu stron
	Gosub Reshow
	return
}
If (To = "")
{
	msgbox Nie podano końca zakresu stron
	Gosub Reshow
	return
}
SplitPath, Name , OutFileName, OutDir, OutExtension, OutNameNoExt, OutDrive
Run, %ComSpec% /c "%USERPROFILE%\pdftotext\pdftotext.exe -layout -f %From% -l %To% %Name%", min
Run, %OutDir%
Gui, PDF:Destroy
return

Reshow:
Gui, PDF:Show, x550 y400 AutoSize, Konwersja PDF na tekst
GuiControl,,Name,%Name%
GuiControl,,From,%From%
GuiControl,,To,%To%
return

Anuluj:
Gui, PDF:Destroy
return

;; inicjały
^#A:: Send {space}A.
^#B:: Send {space}B.
^#C:: Send {space}C.
^#D:: Send {space}D.
^#E:: Send {space}E.
^#F:: Send {space}F.
^#G:: Send {space}G.
^#H:: Send {space}H.
^#I:: Send {space}I.
^#J:: Send {space}J.
^#K:: Send {space}K.
^#L:: Send {space}L.
^#M:: Send {space}M.
^#N:: Send {space}N.
^#O:: Send {space}O.
^#P:: Send {space}P.
^#Q:: Send {space}Q.
^#R:: Send {space}R.
^#S:: Send {space}S.
^#T:: Send {space}T.
^#U:: Send {space}U.
^#V:: Send {space}V.
^#W:: Send {space}W.
^#X:: Send {space}X.
^#Y:: Send {space}Y.
^#Z:: Send {space}Z.



^+d:: ;; otwórz DOI
file = ""
Send ^c
Sleep, 50
file = https://dx.doi.org/%Clipboard% 
;MsgBox, %STR%
Run, %file% 

; $client = new-object System.Net.WebClient
; $client.DownloadFile("http://www.xyz.net/file.txt","C:\tmp\file.txt")
return


^+enter::  ;; wstawienie Entera w miejsce kursora
Clipboard = `n
Send ^v
return


F6:: ;; usuń numerację z bibliografii
Biblio := RegExReplace(Clipboard, "^\s*[0-9]+\.")
Biblio := RegExReplace(Biblio, "\n\s*[0-9]+\.\s*", "`n")
Gui, Font, s10
Gui, Add, Edit,vMyEdit h600 w1180, %Biblio%
Gui, Add, Button, x1080 y625 w100  h20 vSetBiblio gSetBiblio, OK
Gui, Add, Button, x950 y625 w100  h20 gReSetBiblio, Anuluj
GuiControl, Focus, vSetBiblio
Gui, Show,Center W1200 H650, Usunięte numery
return

SetBiblio:
GuiControlGet, MyEdit
Clipboard=%MyEdit%
Gui, Destroy
return

ReSetBiblio:
Gui, Destroy
return



^+LButton:: ;; zaznacz całe pole
SetMouseDelay, 0
MouseGetPos, StartX, StartY
Click, StartX, StartY
Send ^{Home}+^{End}
return

F10:: ;; wyszukiwanie zaznaczonego tekstu w googlu
Send ^c
Sleep, 50
Run, https://www.google.com/search?client=opera&q=%Clipboard%
return


^+u:: ;; uppercase
Send ^{home}
Send ^+{End}
Send ^c
Sleep, 50
Length := StrLen(Clipboard)
StringUpper, Clipboard, Clipboard
Send ^v
Sleep, 50
Send +{Left %Length%}
return


^+l:: ;; lowercase
Send ^{home}
Send ^+{End}
Send ^c
Sleep, 50
Length := StrLen(Clipboard)
StringLower, Clipboard, Clipboard
Send ^v
Sleep, 50
Send +{Left %Length%}
return


^+s:: ;; sentencecase
Send ^{home}
Send ^+{End}
Send ^c
Sleep, 50
Length := StrLen(Clipboard)
First := SubStr(Clipboard, 1 , 1)
Rest := SubStr(Clipboard, 2)
StringUpper, First, First
StringLower, Rest, Rest
Clipboard = %First%%Rest%
Send ^v
Sleep, 50
Send +{Left %Length%}
return

^+bs::
Send ^a
Send ^x
Sleep, 50
tekst := StrReplace(Clipboard, "`r`n`r`n", "@@")
tekst := StrReplace(tekst, "`r`n", " ")
tekst := StrReplace(tekst, "@@", "`r`n")
Clipboard = %tekst%
Send ^v
return

^!bs::
Send ^c
Sleep, 50
tekst := RegExReplace(Clipboard, "-\s*\r\n\s*", "")
tekst := StrReplace(tekst, "`r`n`r`n", "@@")
tekst := StrReplace(tekst, "`r`n", " ")
tekst := StrReplace(tekst, "@@", "`r`n")
Clipboard = %tekst%
Send ^v
return

;; titlecase
^+t::
Send ^{home}
Send ^+{End}
Send ^c
Sleep, 50
Length := StrLen(Clipboard)
StringLower, Clipboard, Clipboard, T
Send ^v
Sleep, 50
Send +{Left %Length%}
return

^+x::
addLatex:
Send ^c
Sleep, 200
wstawka := InStr(Clipboard, "\text{}")
if (wstawka <> 0)
{
	return
}
str := RegExReplace(Clipboard, "_([^{])", "_{$1}")
str := RegExReplace(str, "\^([^{])", "^{$1}")
str := RegexReplace(str, "_\{([^\/]{1,2})/([^}]{1,2})\}", "_{$1»\text«»_«/»\text«»_«$2} ", U)
str := RegexReplace(str, "\^\{([^/]{1,2})/([^}]{1,2})\}", "^{$1»\text«»^«/»\text«»^«$2} ", U)
str := RegexReplace(str, "(\^\{[^}]*\})(\_\{[^}]*\})", "$2$1")
str := RegExReplace(str, "([\^_]{[^{]*})" , Replacement := "$$\text{}$1$$")
str := StrReplace(str, "$$\text{}","")
str := StrReplace(str, "«", "{")
str := StrReplace(str, "»", "}")
Clipboard = %str%
Sleep, 100
Send ^v
return

tuHelp:
Gui, Font, s10, Consolas  ; Set 10-point Verdana
myvar=
(
Klikmania - skróty klawiszowe

Ctrl + Shift + Lewy klawisz myszy - zaznacz całe pole

Zmiana wielkości liter:
Ctrl + Shift + L - LowerCase
Ctrl + Shift + U - UpperCase
Ctrl + Shift + S - SentenceCase - pierwsza litera wielka, reszta małe
Ctrl + Shift + T - TitleCase - każda pierwsza litera wyrazu wielka

Ctrl + Win + litera - Wstawia inicjał: spacja, wielka litera i kropka

Ctrl + Shift + B - Bibliografia - pobranie tekstu z PDF i zapisanie w pliku tekstowym
Ctrl + Shift + D - otwórz DOI - zaznacz id i wciśnij kombinację klawiszy
Ctrl + Shift + X - wstawka TeX - zaznacz tekst zawierający wstawki TeXowe i wciśnij kombinację klawiszy
Ctrl + Shift + BS- Usuń zakończenia wierszy w całym polu. Puste linie zostają.
Ctrl +  Alt  + BS- Usuń zakończenia wierszy w zaznaczonym fragmencie. Puste linie zostają.
F6               - usuń numerację z bibliografii - skopiuj bibliografię do schowka i wciśnij F6
F10              - wyszukaj zaznaczony tekst w Googlu. Przydaje się do wyszukiwania uczelni.

Ctrl + Shift + Enter  - Wstaw Enter w polu abstraktu 

Wyłączenie programu - kliknij prawym przyciskiem myszy ikonkę programu w prawym dolnym rogu ekranu i wybierz Exit

ver. 0.12
)
Gui, Add, Text,,%myvar%
Gui, Show,AutoSize, Skróty klawiszowe  
return


GuiClose:
Gui, Destroy
return

RandomText(len:=100, count:=100)
{
s := (s := "0,1,2,3,4,5,6,7,8,9,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z") "," s "," s
Sort, s, Random D,
i := SubStr(StrReplace(s, ","), 1, 4)
MsgBox, % i
}