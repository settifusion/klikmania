# klikmania - pomoc dla klikaczy

Program zawierający funkcje pomocne dla klikaczy operacji - usuwanie numerów, formatowanie itp.

## Aktualna wersja
### 0.12
Dodane inicjały

### 0.11
Dodany skrót do wyszukiwarki

### 0.10
Dodane usuwanie enterów z zaznaczonego tekstu (łączenie linii)

### 0.91
Program jest tworzony w miarę potrzeb i dostępnego czasu. Proszę o wyrozumiałość.

Wszelkie uwagi i pomoc przy wyłapywaniu pluskiew mile widziane.

## Skróty klawiszowe
```
Ctrl + Shift + Lewy klawisz myszy - zaznacz całe pole

Zmiana wielkości liter:
Ctrl+Shift+ L - LowerCase
Ctrl+Shift+ U - UpperCase
Ctrl+Shift+ S - SentenceCase - pierwsza litera wielka, reszta małe
Ctrl+Shift+ T - TitleCase - każda pierwsza litera wyrazu wielka

Ctrl + Win + litera - Wstawia inicjał: spacja, wielka litera i kropka

Ctrl+Shift+ B - Bibliografia pobranie tekstu i zapisanie w pliku tekstowym
Ctrl+Shift+ D - otwórz DOI - zaznacz id i wciśnij kombinację klawiszy
Ctrl+Shift+ X - wstawka TeX - zaznacz tekst zawierający wstawki TeXowe i wciśnij kombinację klawiszy
Ctrl+Shift+ BS- Usuń zakończenia wierszy w całym polu. Puste linie zostają.
F6            - usuń numerację z bibliografii - skopiuj bibliografię i wciśnij F6
F10           - wyszukaj zaznaczony tekst w Googlu. Przydaje się do wyszukiwania uczelni.


Ctrl + Shift + Enter  - Wstaw Enter w polu abstraktu 
```
Wyłączenie programu - kliknij prawym przyciskiem myszy ikonkę programu w prawym dolnym rogu ekranu i wybierz Exit.

## Funkcje programu

### Zmiana wielkości liter w polu
Program umożliwia automatyczną zmianę wielkości na:

- wszystkie małe (Ctrl+Shift+ L)
- wszystkie duże (Ctrl+Shift+ U)
- każde słowo zaczyna się wielką literą (Ctrl+Shift+ T)
- jak w zdaniu - pierwsza litera wielka, reszta małe (Ctrl+Shift+ S). Kropka nie jest dostawiana :-)

Uwaga - funkcja jest niewrażliwa na nazwy, nazwiska, inicjały itp. Wszystkie słowa są traktowane jednakowo.

1. Ustaw kursor w w polu, które ma niewłaściwe wielkości liter.
1. Przyciśnij kombinację klawiszy.
1. Całe pole zostanie zaznaczone, po czym zostanie zmieniona wielkość liter. Operacja zawsze obejmuje całe pole, w którym jest kursor.

### Pobranie tekstu z PDF i zapisanie w pliku tekstowym
Pobieranie tekstu z PDF za pomocą narzędzia `pdftotext`. Z doświadczenia z wieloma dokumentami wynika, że jest to metoda dająca najlepsze rezultaty. Zdarzają się jednak przypadki, kiedy ta metoda zawodzi, a lepszy wynik wychodzi przy użyciu schowka czy innego programu. Niestety, tego nie da się określić na 100%.

Funkcja powstała do pobierania bibliografii z plików PDF, ale może służyć do pobierania dowolnych fragmentów np. indeksów albo spisów treści czy wręcz całego tekstu publikacji. Zależy to tylko od zakresu podanych stron. 

Podanie numerów stron jest obowiązkowe.

1. Zapisz plik PDF na dysku (w dowolnym folderze, ale musisz wiedzieć, gdzie).
1. Zorientuj się gdzie zaczyna się i kończy bibliografia. Przydatny bywa tu spis treści.
1. Przyciśnij kombinację klawiszy `Ctrl+Shift+B`.
1. Pojawi się okienko z trzema polami. Kliknij przycisk `Plik` znajdujący się w lewym górnym rogu okienka. 
1. Wybierz plik, z którego ma zostać pobrany tekst. Ścieżka do pliku zostanie wpisana do pierwszego pola okienka.
1. Podaj zakres stron, wpisując pierwszą i ostatnią stronę, na której znajduje się bibliografia. 
1. Kliknij przycisk `Konwertuj`.
1. Tekst zostanie pobrany z dokumentu i zapisany w tym samym katalogu, co PDF. Nazwa pliku z tekstem jest taka sama jak dokumentu PDF, ma jedynie rozszerzenie .txt. Zostanie otwarte okno menedżera plików z folderem zawierającym plik źródłowy i pobrany tekst. Można go otworzyć w ulubionym edytorze.

### Usunięcie zakończeń wierszy w całym polu
Szczególnie przydaje się przy abstraktach, które niekiedy są wklejane sformatowane na szerokość kilkudziesięciu kolumn. Jeśli chcemy pozostawić jakieś przełamanie linii (zakończenie akapitu) trzeba w tym miejscu wstawić pustą linię.

1. Powiększ pole (ciągnąc za prawy dolny róg), by zobaczyć całość, a przynajmniej większość tekstu.
1. Wstaw puste linie w miejscach, które powinny być zakończeniem akapitu - akapity powinny być oddzielone dodatkowym wierszem.
1. Ustaw kursor w polu.
1. Wciśnij kombinację `Ctrl+Shift+BackSpace`.
1. Sprawdź, czy wynik jest zgodny z oczekiwaniem.

### Skrót do wyszukiwwarki

1. Zaznacz tekst gdziekolwiek (w oknie przeglądarki, edytorze)
1. Przyciśnij klawisz `F10`

Tekst zostanie wyszukany w Googlach. Bardzo się to przydaje przy wyszukiwaniu nazw jednostek naukowych, szczególnie pisanych w innych językach.